<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('cast')->get();
        return view('cast.cast',['data'=>$data]);
    }

    public function create(){
        return view('cast/add_cast');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'inputan nama harus diisi',
                'umur.required' => 'inputan umur harus diisi',
                'bio.required' => 'inputan bio harus diisi',
            ]);
        // $data = array(
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio,
        // );
        DB::table('cast')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('cast')->where('id',$id)->first();
        return view('cast.show_cast',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $data = DB::table('cast')->where('id',$id)->first();
        return view('cast.edit_cast',['data' => $data]);
    }
    public function update(Request $request, $id)
    {
        $validateData = $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'inputan nama harus diisi',
                'umur.required' => 'inputan umur harus diisi',
                'bio.required' => 'inputan bio harus diisi',
            ]);
        // $data = array(
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio,
        // );
        DB::table('cast')->where('id',$id)->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Db::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
}
