<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form(){
        return view('form');
    }
    public function welcome(Request $request){
        $namaDepan = $request->fname;
        $namaBelakang = $request->lname;
        return view('welcome',['namaDepan'=>$namaDepan,'namaBelakang'=>$namaBelakang]);
    }
}
