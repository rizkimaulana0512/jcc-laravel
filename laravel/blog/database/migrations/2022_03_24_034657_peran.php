<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Peran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama_peran');
            $table->unsignedBigInteger('cast_id');
            $table->unsignedBigInteger('film_id');
        });

        Schema::table('peran',function (Blueprint $table){
            $table->foreign('cast_id')->references('id')->on('cast')->onDelete('cascade');
            $table->foreign('film_id')->references('id')->on('film')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
