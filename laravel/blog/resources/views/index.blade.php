@extends('temp/temp')
    <head>
        <title>
            @section('title')
                Index Page
            @stop
        </title>
    </head>
    @section('content')
        
        <h1>Media Online</h1>
        
        <h3>Social Media Developer</h3>
        
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        
        <h3>Benefit Join di Media Online</h3>
        <ul>
            <li>Mendapatkan motivasi dari sesama</li>
            <li>Sharing knowledge</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
        
        <h3>Cara Bergabung ke Media Online</h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftarkan di <a href="/form">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>
    @stop