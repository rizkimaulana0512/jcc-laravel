@extends('temp/temp')
@section('title')
  Halaman List Cast
@stop
@section('content')
  <a href="/cast/create" class="btn btn-primary">Tambah Cast</a>
        <table class="table">
          <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($data as $key => $value)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                  <form action="/cast/{{$value->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>
                </td>
              </tr>
            @empty
              <h1>Data Kosong</h1>
            @endforelse
          </tbody>
        </table>
@stop