@extends('/temp/temp')
    @section('title')
        Form Page
    @stop
    @section('content')
        <form action="/cast/{{$data->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-grop">
               <label>Nama</label>
               <input type="text" class="form-control" name="nama" value="{{$data->nama}}">
            </div>
            @error('nama')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <div class="form-grop">
               <label>Umur</label>
               <input type="number" class="form-control" name="umur" value="{{$data->umur}}">
            </div>
            @error('umur')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <div class="form-grop">
               <label>Bio</label>
               <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{$data->bio}}</textarea>
            </div>
            @error('bio')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @stop