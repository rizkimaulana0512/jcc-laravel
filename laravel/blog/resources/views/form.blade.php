@extends('temp/temp')
    @section('title')
        Form Page
    @stop
    @section('content')
        <h2>Buat Account Baru</h2>    
        <p>
            <b>Sign Up Form</b>
        </p>
        <form action="/welcome" method="post">
            @csrf
            <p>First name :</p>
            <input type="text" name="fname" id="fname">
            <p>Last name :</p>
            <input type="text" name="lname" id="lname">
            <p>Gender</p>
            <p>
                <input type="radio" value="Male" name="gender"> Male
            </p>
            <input type="radio" value="Femail" name="gender"> Female
            <p>Nationality </p>
            <select name="nat" id="nat">
                <option value="Indonesia">Indonesia</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select>
            <p>Language Spoken</p>
            <input type="checkbox" name="lang" id="lang"> Bahasa Indonesia <br>
            <input type="checkbox" name="lang" id="lang"> English <br>
            <input type="checkbox" name="lang" id="lang"> Other <br>
            <p>Bio</p>
            <textarea name="bio" id="" cols="25" rows="8"></textarea><br>
            <button type="submit">Sign Up</button>
        </form>
    @stop