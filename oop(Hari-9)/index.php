<?php
require('Animal.php');
require('Ape.php');
require('Frog.php');

function output($n){
    echo "Name : $n->name<br>";
    echo "Legs : $n->legs<br>";
    echo "Cold Bloded : $n->cold_bloded<br>";
    
}
echo "<h3>Release 0 </h3>";
$sheep = new Animal("shaun");
output($sheep);

echo "<h3>Release 1</h3>";
$sungokong = new Ape("kera sakti");
output($sungokong);
$sungokong->yell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
output($kodok);
$kodok->jump() ; // "hop hop"
?>